from HTMLParser import HTMLParser
import urllib2


class MyHTMLParser(HTMLParser):
    ''' HTML parser: parses <pre> </pre> tags'''
    def __init__(self):
        HTMLParser.__init__(self)
        self.recording = 0
        self.data = []

    def handle_starttag(self, tag, attrs):
        if tag == 'pre':
                    self.recording = 1 

    def handle_endtag(self,tag):
        if tag == 'pre':
            self.recording -= 1

    def handle_data(self, data):
        if self.recording:
            self.data=data.split('\n')
        self.data=map(lambda x: x.split('  '), self.data)
        self.data=filter(lambda x: len(x)>2, self.data)




class Durbin_Watson_Table(object):
    ''' exctracts values for the table from html files '''
    def __init__(self,urls):
        self.table = []
        for url in urls:
            y = urllib2.urlopen(url)
            html = y.read()
            parser = MyHTMLParser()
            parser.feed(html)
            self.table+=parser.data
            parser.close()
            
dw=Durbin_Watson_Table(
    [
        'http://web.stanford.edu/~clint/bench/dw05a.htm',
        'http://web.stanford.edu/~clint/bench/dw05b.htm',
        'http://web.stanford.edu/~clint/bench/dw05c.htm',
        'http://web.stanford.edu/~clint/bench/dw05d.htm'
    ]

)

print dw.table
