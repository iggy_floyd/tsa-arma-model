from numpy.random import standard_normal
from numpy import array, zeros, sqrt, shape
from pylab import *


def ar1(x_0=1e3,sudden_day=50.,T=100,dt=1e0, alpha=0., sigma=1e1):
    ''' generate the ar(1) process '''
    
    
    Steps=round(T/dt); #Steps in years
    S = zeros([Steps], dtype=float)
    x = range(0, int(Steps), 1)

    
    S[0] = 0.
    for i in x[:-1]:
        if ((S[i] == 0.) and  (x[i]>sudden_day)   ):
            S[i] = x_0
        if (x[i]<=sudden_day):
             S[i+1] =0.
        else:
            if (S[i]>=1e-1*sigma):
                S[i+1]=S[i]*alpha+sigma*standard_normal();
            else:
                S[i+1] = 1e-2
    
    plot(x, S,label='bottles sold')
    xlabel('time (days)')
    ylabel('sold bottles')
    legend(loc="upper left")
    show()    
    return S
    

# unusual  hit of temperature on 50th day
ar1_data=ar1(x_0=1e3,sudden_day=50.,T=100,dt=1e0, alpha=0.5, sigma=1e2)
