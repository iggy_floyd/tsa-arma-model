from numpy.random import standard_normal
from numpy import array, zeros, sqrt, shape
from pylab import *


def ma1(x_0=1e3,error_0=1e2,sudden_day=50.,T=100,dt=1e0, alpha=0., sigma=1e1):
    ''' generate the ma(1) process '''
    
    
    Steps=round(T/dt); #Steps in years
    S = zeros([Steps], dtype=float)
    error = zeros([Steps], dtype=float)
    
    x = range(0, int(Steps), 1)

    
    
    S[0] = 0.
    error[0] = 0.
    for i in x[:-1]:
        if ((S[i] == 0.) and  (x[i]>sudden_day)   ):
            S[i] = x_0
            error[i] = error_0
        if (x[i]<=sudden_day):
            S[i+1] =0.
            error[i+1] =0.
        else:
            if (S[i]>=1e-1*sigma):
                error[i+1] = sigma*standard_normal()
                S[i+1]=error[i]*alpha+error[i+1];
        
            else:
                S[i+1] = 1e-2
                error[i+1] = 0.
    
    plot(x, S,label='bags sold')
    xlabel('time (days)')
    ylabel('sold bags')
    legend(loc="upper left")
    show()    
    return S


# 
ma1_data=ma1(x_0=1e3,error_0=1e2,sudden_day=50.,T=100,dt=1e0, alpha=0.5, sigma=1e1)
