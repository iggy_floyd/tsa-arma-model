# generation of the ARMA Process
from statsmodels.tsa.arima_process import arma_generate_sample, ArmaProcess



np.random.seed(1234)

def arma_generate(arparams, maparams):
    ''' produce arma process '''
    
    arma_t = ArmaProcess(arparams, maparams)
    fig = plt.figure(figsize=(12,8))
    ax = fig.add_subplot(111)
    ax.plot(arma_t.generate_sample(250));
    return arma_t



# include zero-th lag

arparams = np.array([1, .785, .65])
maparams = np.array([1, .65])

arma_data = arma_generate(arparams,maparams)
