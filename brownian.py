from numpy.random import standard_normal
from numpy import array, zeros, sqrt, shape
from pylab import *


def brownian(x_0=1e-9,T=100,dt=1e0, mu=0., sigma=1e-2, N_Sim=10,geometric=True):
    ''' generate the geometric brownian  motion (GBM) '''
    
    
    Steps=round(T/dt); #Steps in years
    S = zeros([N_Sim, Steps], dtype=float)
    x = range(0, int(Steps), 1)

    for j in range(0, N_Sim, 1):
        S[j,0]= x_0
        for i in x[:-1]:
            if (geometric):
                S[j,i+1]=S[j,i]+S[j,i]*(mu-0.5*pow(sigma,2))*dt+sigma*S[j,i]*sqrt(dt)*standard_normal();
            else:   
                S[j,i+1]=S[j,i]+ sigma*sqrt(dt)*standard_normal()
        plot(x, S[j],label='asset %i'%j)
    title(' %d Simulations of %d  Days with parameters: (Sigma %.6f, Mu %.6f, S0 %.6f)' % (int(N_Sim), int(Steps), sigma, mu, x_0))
    xlabel('time (days)')
    ylabel('stock price')
    legend(loc="upper left")
    show()    


sigma = 1e-2
mu=0.

brownian(x_0=1e-4,T=100.,dt=1e0, mu=mu, sigma=sigma, N_Sim=5,geometric=False)
