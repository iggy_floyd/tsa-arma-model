from numpy import cumsum, log, polyfit, sqrt, std, subtract

def HurstExponent(ts,lag_range=100):
    ''' calculates the Generaliyed Hurst Exponent on the Time Series 'ts' '''
    
     # Create the range of lag values
    ts_size = len(ts)
    lags = range(2, lag_range if lag_range <= ts_size else ts_size )
    
    
    # Calculate the array of the variances of the lagged differences (S_2)
    tau = [sqrt(std(subtract(ts[lag:], ts[:-lag]))) for lag in lags]
    
    # Use a linear fit to estimate the Hurst Exponent
    poly = polyfit(log(lags), log(tau), 1)
    
    # Return the Hurst exponent from the polyfit output
    return poly[0]*2.0

# test of HurstExponent


# Create a Gometric Brownian Motion, Mean-Reverting and Trending Series
gbm = log(cumsum(randn(100000))+1000)
mr = log(randn(100000)+1000)
tr = log(cumsum(randn(100000)+1)+1000)


# Output the Hurst Exponent for each of the above series
# and the price of Google (the Adjusted Close price) for 
# the ADF test given above in the article
print "Hurst(GBM):   %s" % HurstExponent(gbm)
print "Hurst(MR):    %s" % HurstExponent(mr)
print "Hurst(TR):    %s" % HurstExponent(tr)


def HurstStationaryTest(ts,lag_range=100):
    ''' makes a test on the stationarity 
        returns 0 in a case of a Gometric Brownian Motion
        returns 1 in a case of a Mean-Reverting Motion
        returns 2 in a case of a Trending Motion
        returns -1 in a case of some problem
    
    '''
    
    
    hurst_expo = HurstExponent(ts,lag_range)
    if abs(hurst_expo-0.50)<0.25: return 0
    if hurst_expo<0.50: return 1
    if hurst_expo>0.50: return 2
    
    return -1
